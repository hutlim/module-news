<% include SideBar %>
<div class="typography col-sm-8">
    <div class="well">
        $Message
    	<article>
    		<h1>$Title</h1>
    		<div class="row">
    			<% if $News %>
    				<div id="news-list">
						<% loop $News %>
						    <div class="col-sm-6 col-md-4 col-lg-3 news">
							    <div class="thumbnail">
							    	<% if $NewsGalleries.count %>
							      		<img class="img-responsive" width="100%" alt="$NewsGalleries.First.Title" src="$NewsGalleries.First.NewsImage.CroppedImage(640,427).URL" />
							    	<% else %>
							    		<img class="img-responsive" width="100%" alt="$Title" src="/news/images/news-banner.gif" />
							      	<% end_if %>
							      	<div class="caption">
							        	<h3 class="title">$Title</h3>
							        	<p class="summary"><i class="fa fa-calendar-o"></i> $PublishDate.Nice $Content.ContextSummary(200)</p>
							        	<p class="text-right"><a href="$Link" class="btn btn-primary btn-sm"><%t NewsPage.ss.MORE "More.." %></a></p>
							      	</div>
							    </div>
						    </div>
						<% end_loop %>
					</div>
				<% else %>
					<div class="list-group-item">
						<p class="list-group-item-text"><%t NewsPage.ss.NO_NEWS "No news at this moment" %></p>
					</div>
				<% end_if %>
			</div>
			<% if $News.MoreThanOnePage %>
	            <div class="text-center">
	                <ul class="pagination">
		                <% if $News.NotFirstPage %>
		                    <li><a href="$News.PrevLink"><%t NewsPage.ss.PREV "Prev" %></a></li>
		                <% else %>
		                    <li class="disabled"><a href="#"><%t NewsPage.ss.PREV "Prev" %></a></li>
		                <% end_if %>
		                <% loop $News.Pages(5) %>
		                    <% if CurrentBool %>
		                        <li class="active"><a href="#">$PageNum</a></li>
		                    <% else %>
		                        <% if Link %>
		                            <li><a href="$Link">$PageNum</a></li>
		                        <% else %>
		                            <li><a href="#">...</a></li>
		                        <% end_if %>
		                    <% end_if %>
		                <% end_loop %>
		                <% if $News.NotLastPage %>
		                    <li><a href="$News.NextLink"><%t NewsPage.ss.NEXT "NEXT" %></a></li>
		                <% else %>
		                    <li class="disabled"><a href="#"><%t NewsPage.ss.NEXT "Next" %></a></li>
		                <% end_if %>
	                </ul>
	            </div>
	        <% end_if %>
    	</article>
    </div>
</div>