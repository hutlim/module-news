<?php
/**
 * @package news
 */
class News extends DataObject implements PermissionProvider {
    private static $singular_name = "News";
    private static $plural_name = "News";
    
    private static $db = array(
    	'PublishDate' => 'Date',
    	'IsActive' => 'Boolean',
    	'Title' => "Varchar(250)",
        'Content' => 'HTMLText'
    );
	
	private static $has_many = array(
		'NewsGalleries' => 'NewsGallery'
	);
	
	private static $many_many = array(
        'ViewGroups' => 'Group'
    );
	
	private static $defaults = array(
		'IsActive' => 1
	);
	
	private static $default_sort = "PublishDate DESC";

    private static $searchable_fields = array(
    	'PublishDate' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'IsActive',
        'Title',
        'Content'
    );

    private static $summary_fields = array(
    	'PublishDate.Nice',
    	'IsActive.Nice',
        'Title',
        'Content.Summary',
        'ViewGroupsText'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['PublishDate'] = _t('News.PUBLISHED_DATE', 'Published Date');
		$labels['PublishDate.Nice'] = _t('News.PUBLISHED_DATE', 'Published Date');
		$labels['IsActive'] = _t('News.IS_ACTIVE', 'Is Active?');
		$labels['IsActive.Nice'] = _t('News.IS_ACTIVE', 'Is Active?');
		$labels['Title'] = _t('News.TITLE', 'Title');
		$labels['Content'] = _t('News.CONTENT', 'Content');
		$labels['Content.Summary'] = _t('News.CONTENT', 'Content');
		$labels['NewsGalleries'] = _t('News.NEWS_GALLERIES', 'News Galleries');
		$labels['ViewGroups'] = _t('News.VIEW_GROUPS', 'View Groups');
		$labels['ViewGroupsText'] = _t('News.VIEW_GROUPS', 'View Groups');
		
		return $labels;	
	}
	
	function getCMSFields() {
        $fields = parent::getCMSFields();

		if($this->exists()) {
			$gridFieldConfig = GridFieldConfig_RecordEditor::create()
			->removeComponentsByType('GridFieldAddNewButton')
			->removeComponentsByType('GridFieldAddExistingAutocompleter')
			->removeComponentsByType('GridFieldPaginator')
            ->removeComponentsByType('GridFieldPageCount')
			->addComponents(new GridFieldBulkUpload(), new GridFieldSortableRows('Sort'), new GridFieldGalleryTheme('NewsImage'));
			$gridFieldConfig->getComponentByType('GridFieldBulkUpload')->setUfSetup('setFolderName', 'NewsGallery');
			$fields->dataFieldByName('NewsGalleries')->setConfig($gridFieldConfig);
		}
		
		$groups = Group::get()->filter('IsDistributorGroup', 1);
		
		$fields->removeByName('ViewGroups');
    	$fields->addFieldToTab('Root.Main', $group_list = ListboxField::create('ViewGroups', $this->fieldLabel('ViewGroups'), $groups->map('ID', 'Breadcrumbs')->toArray())->setMultiple(true));
		
		if(!$this->exists()){
			$group_list->setValue(array_values($groups->getIDList()));
		}
        
        return $fields;
    }
	
	function Link(){
		$page = NewsPage::get_one('NewsPage');
		if($page){
			return Controller::join_links($page->Link('view'), $this->ID);
		}
		
		return '#';
	}
	
	function ViewGroupsText(){
		return implode(', ', $this->ViewGroups()->map()->toArray());
	}
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_News');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_News');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_News');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_News');
    }

    public function providePermissions() {
        return array(
            'VIEW_News' => array(
                'name' => _t('News.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('News.PERMISSIONS_CATEGORY', 'News')
            ),

            'EDIT_News' => array(
                'name' => _t('News.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('News.PERMISSIONS_CATEGORY', 'News')
            ),

            'DELETE_News' => array(
                'name' => _t('News.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('News.PERMISSIONS_CATEGORY', 'News')
            ),

            'CREATE_News' => array(
                'name' => _t('News.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('News.PERMISSIONS_CATEGORY', 'News')
            ),
        );
    }
}
?>