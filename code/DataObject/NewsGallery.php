<?php
class NewsGallery extends DataObject {
    private static $singular_name = "News Gallery";
    private static $plural_name = "News Galleries";
	
	private static $db = array(
		'Title' => 'Varchar(250)',
		'Sort' => 'Int'
	);
	
	private static $has_one = array(
		'NewsImage' => 'Image',
		'News' => 'News'
	);
	
	function canCreate($member = null) {
		return(Permission::checkMember($member, array('CREATE_NEWS', 'CMS_ACCESS_NewsAdmin')));
	}
	
	function canEdit($member = null){
		return(Permission::checkMember($member, array('EDIT_NEWS', 'CMS_ACCESS_NewsAdmin')));
	}
	
	function canDelete($member = null) {
		return(Permission::checkMember($member, array('DELETE_NEWS', 'CMS_ACCESS_NewsAdmin')));
	}
	
	function canView($member = null) {
		return(Permission::checkMember($member, array('VIEW_NEWS', 'CMS_ACCESS_NewsAdmin')));
	}
}
?>