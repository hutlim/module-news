<?php
class NewsImage extends Image {
    private static $singular_name = "News Image";
    private static $plural_name = "News Images";
	
	function canEdit($member = null) {
        return true;
    }
    
    function canView($member = null) {
        return true;
    }
    
    function canDelete($member = null) {
        return true;
    }

    function canCreate($member = null) {
        return true;
    }
}
?>