<?php

class News_ControllerExtension extends Extension {
	function getNewsPage(){
        $page = NewsPage::get()->find('ClassName', 'NewsPage');
        if($page) return $page;
    }
    
    function getNewsLink() {
        $page = $this->getNewsPage();
        if($page) return $page->Link();
    }
}
