<?php
/**
 * News administration interface, based on ModelAdmin
 * @package news
 */
class NewsAdmin extends GeneralModelAdmin {

    private static $url_segment = 'news';
    private static $menu_title = 'News';
    private static $menu_icon = 'news/images/news-icon.png';

    private static $managed_models = array(
    	'News'
    );
	
	public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        $listField = GridField::create(
            $this->sanitiseClassName($this->modelClass),
            false,
            $list,
            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                ->removeComponentsByType('GridFieldFilterHeader')
                ->addComponents(new NewsGridFieldActivateAction(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
        );
		
		if(Permission::check('EDIT_Announcement') || Permission::check('DELETE_Announcement')){
			$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
			if(Permission::check('EDIT_News')){
				$bulkButton
				->addBulkAction('activate', _t('NewsAdmin.ACTIVATE', 'Activate'), 'GridFieldBulkNewsHandler')
				->addBulkAction('deactivate', _t('NewsAdmin.DEACTIVATE', 'Deactivate'), 'GridFieldBulkNewsHandler', array('icon' => 'cross-circle'));
			}
			
			if(Permission::check('DELETE_News')){
				$bulkButton->addBulkAction('delete', _t('NewsAdmin.DELETE', 'Delete'), 'GridFieldBulkNewsHandler', array('icon' => 'decline'));
			}
		}
        
        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
            $listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
?>