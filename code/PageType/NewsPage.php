<?php
class NewsPage extends MemberOverviewPage {
	
    private static $db = array();

    private static $has_one = array();

}

class NewsPage_Controller extends MemberOverviewPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array (
        'view'
    );
	
	public function init(){
		parent::init();
		Requirements::javascript('news/javascript/NewsPage.min.js');
		Requirements::css('news/css/NewsPage.css');
	}
	
	public function News(){
		$news = News::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->CurrentMember()->RankID);
		
		return new PaginatedList($news, $this->request);
	}
    
    public function view(){
        if($news = News::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->CurrentMember()->RankID)->byID((int)$this->request->param('ID'))){
            return array(
            	'News' => $news
            );
        }
        
        return $this->httpError('404', 'Page not found');
    }
}